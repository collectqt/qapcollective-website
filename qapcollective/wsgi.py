"""
WSGI config for qapcollective project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys

app_path = os.path.dirname(os.path.abspath(__file__))
lib_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), os.path.pardir, '.venv/lib/python3.4/site-packages/')
sys.path.append(app_path)
sys.path.append(lib_path)

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "qapcollective.settings")

application = get_wsgi_application()
